package sample;

import com.sun.org.apache.xalan.internal.xsltc.runtime.Operators;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class Controller<string> {
    public Label monitor;
    public Button btnAC;
    public Button btn7;
    public Button btn4;
    public Button btn1;
    public Button btn0;
    public Button btnSign;
    public Button btn8;
    public Button btn5;
    public Button btn2;
    public Button btnPercent;
    public Button btn9;
    public Button btn6;
    public Button btn3;
    public Button btnDot;
    public Button btnDivide;
    public Button btnMultiply;
    public Button btnMinus;
    public Button btnPlus;
    public Button btnEquals;
    public double a = 0,b = 0;
    public String c = "";

    public void clickAC(ActionEvent actionEvent) {
        monitor.setText("");
    }

    public void clickNUM(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        String num = monitor.getText();
        monitor.setText(num.concat(button.getText()));
    }

    public void clickCOMPUTE(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        switch (button.getText()){
            case "+":
                c = "+";
                a = Double.parseDouble(monitor.getText());
                monitor.setText("");
                break;
            case "-":
                c = "-";
                a = Double.parseDouble(monitor.getText());
                monitor.setText("");
                break;
            case "*":
                c = "*";
                a = Double.parseDouble(monitor.getText());
                monitor.setText("");
                break;
            case "/":
                c = "/";
                a = Double.parseDouble(monitor.getText());
                monitor.setText("");
                break;
            case "0/0":
                c = "%";
                a = Double.parseDouble(monitor.getText());
                monitor.setText("");
                break;
            case "+/-":
                a = Double.parseDouble(monitor.getText());
                monitor.setText(String.valueOf(a*-1));
        }
    }

    public void clickEQUALS(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        switch (button.getText()){
            case "=":
                b = Double.parseDouble(monitor.getText());
                if (c == "+"){
                    monitor.setText(String.valueOf(a+b));
                }
                else if (c == "-"){
                    monitor.setText(String.valueOf(a-b));
                }
                else if (c == "*"){
                    monitor.setText(String.valueOf(a*b));
                }
                else if (c == "/"){
                    monitor.setText(String.valueOf(a/b));
                }
                else if (c == "%"){
                    monitor.setText(String.valueOf(a%b));
                }
                break;
        }
    }
}
